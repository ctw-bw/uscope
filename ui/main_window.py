from PyQt5.QtWidgets import QWidget, QMainWindow, QLabel, QCheckBox, \
    QLineEdit, QPushButton, QMenu, QAction, QMessageBox, QFileDialog, \
    QVBoxLayout, QHBoxLayout, QFormLayout
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QIcon, QCloseEvent
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
import pyqtgraph as pg
import numpy as np
import json
import os
import time
from typing import Optional, List

from plot_decoder.plot_decoder import PlotDecoder
from ui.combo_box import ComboBox

try:
    import ctypes
    app_id = 'BioRobotics.uScope'  # Change application id to make the
    # taskbar icon displayed correctly
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)
except ImportError:
    ctypes = None  # Very unimportant fix, continue if ctypes was not found
except AttributeError:
    ctypes = None  # On MAC the package might exist but not the feature


class MainWindow(QMainWindow):
    """Class for running the application window"""

    LINECOLORS = ['y', 'm', 'r', 'g', 'c', 'w', 'b']

    FRAME_TIME = 1.0 / 60.0  # Inverse of framerate

    def __init__(self, *args, **kwargs):
        """Constructor"""

        super().__init__(*args, **kwargs)  # Run parent constructor

        # Initialize serial
        self.serial = QSerialPort(
            None,
            baudRate=QSerialPort.Baud115200,
            readyRead=self.on_serial_receive
        )

        self.decoder = PlotDecoder()

        # Prepare data structure
        self.channels = 0  # Wait for serial data, resize on the fly
        self.data: Optional[np.array] = None  # Received data, each row is a channel
        self.time: Optional[np.array] = None  # Timestamps of each data column
        self.data_points = 0  # Number of points recorded
        self.data_size = 200  # Number of points in history
        self.time_offset = None  # Time offset in microseconds
        # When true, all plots should be combined in one plot
        self.overlay = False
        self.autoscale = True  # Automatic y-scaling when true
        self.y_scale = [-10.0, 10.0]  # Y-scale values when not automatic

        # The system time of the last frame update - Used to limit framerate
        self.last_update = 0.0

        # Create property stubs
        self.input_port = ComboBox()
        self.button_port = QPushButton('Connect')
        self.input_size = QLineEdit()
        self.input_overlay = QCheckBox()
        self.input_autoscale = QCheckBox()
        self.input_scale = {
            'min': QLineEdit(),
            'max': QLineEdit()
        }
        self.input_render = QLineEdit()
        self.layout_plots = pg.GraphicsLayoutWidget()
        self.button_save = QPushButton('Save')

        self.plots: List[pg.PlotItem] = []  # Start with empty plots
        self.curves: List[pg.PlotDataItem] = []

        self.build_ui_elements()  # Put actual GUI together

        self.setWindowTitle('uScope')
        icon_path = os.path.realpath('images/logo.ico')
        self.setWindowIcon(QIcon(icon_path))

        self.show()

        self.set_channels(self.channels)

        # Load previous settings
        self.load_settings()

    def build_ui_elements(self):
        """Create and connect the Qt Widgets to build the full GUI"""

        main_widget = QWidget()
        layout_main = QVBoxLayout(main_widget)  # Main vertical layout
        layout_top = QHBoxLayout()  # Layout for top buttons
        layout_bottom = QVBoxLayout()  # Layout for channels

        # Port control
        layout_settings = QFormLayout()
        self.input_port.popupAboutToBeShown.connect(self.find_devices)
        # Call it once already so an initial value is chosen
        self.find_devices()
        layout_settings.addRow(QLabel('Serial port:'), self.input_port)
        self.button_port.setCheckable(True)
        self.button_port.toggled.connect(self.on_connect_toggle)

        # Data size
        self.input_size.setValidator(QIntValidator(5, 1000000))
        self.input_size.setText(str(self.data_size))
        layout_settings.addRow(QLabel('Samples:'), self.input_size)

        # Overlay
        self.input_overlay.setChecked(self.overlay)
        layout_settings.addRow(QLabel('Overlay channels:'), self.input_overlay)

        # Y-Scale
        layout_scaling = QHBoxLayout()
        self.input_autoscale.setChecked(self.autoscale)
        self.input_autoscale.toggled.connect(self.on_autoscale_toggle)
        layout_scaling.addWidget(self.input_autoscale)
        layout_scaling.addWidget(QLabel('Autoscale'))
        layout_scaling.addStretch(0)
        layout_scaling.addWidget(QLabel('Manual scale:'))

        for key, input_scale in self.input_scale.items():
            input_scale.setValidator(QDoubleValidator(-1.0e6, 1.0e6, 4))
            val = self.y_scale[0] if key == 'min' else self.y_scale[1]
            input_scale.setText(str(val))
            input_scale.setDisabled(self.autoscale)
            layout_scaling.addWidget(input_scale)

        layout_settings.addRow(QLabel('Y-scale:'), layout_scaling)

        # Attach top layout
        layout_top.addLayout(layout_settings)
        layout_top.addWidget(self.button_port)

        layout_main.addLayout(layout_top)

        # Plots
        layout_bottom.addWidget(self.layout_plots)

        layout_main.addLayout(layout_bottom)

        # Buttons
        layout_buttons = QHBoxLayout()
        menu_save = QMenu()
        menu_save.addAction('Numpy')
        menu_save.addAction('CSV')

        menu_save.triggered.connect(self.on_save)
        self.button_save.setMenu(menu_save)
        layout_buttons.addWidget(self.button_save)
        layout_main.addLayout(layout_buttons)

        # Main window widget
        self.setCentralWidget(main_widget)

    @pyqtSlot(bool)
    def on_connect_toggle(self, checked: bool):
        """When the serial `connect` button is pressed"""

        self.button_port.setText('Disconnect' if checked else 'Connect')

        self.serial.close()

        if checked:
            port = self.input_port.currentData()
            self.serial.setPortName(port)

            # If serial opened successfully
            if self.serial.open(QSerialPort.ReadOnly):
                self.input_port.setDisabled(True)
                self.input_size.setDisabled(True)
                self.input_overlay.setDisabled(True)
                self.input_autoscale.setDisabled(True)
                self.start_recording()
            else:
                self.button_port.setChecked(False)  # Undo toggle
                message = QMessageBox()
                QMessageBox.warning(message, 'Serial connection',
                                    'Could not connect to device',
                                    QMessageBox.Ok)
        else:
            self.input_port.setDisabled(False)
            self.input_size.setDisabled(False)
            self.input_overlay.setDisabled(False)
            self.input_autoscale.setDisabled(False)

    @pyqtSlot(bool)
    def on_autoscale_toggle(self, checked: bool):
        """Callback for the autoscale checkbox"""

        # Enable/disable manual scales
        for key, input_scale in self.input_scale.items():
            input_scale.setDisabled(checked)

    @pyqtSlot()
    def on_serial_receive(self):
        """"
        Callback for serial data, already triggered by data

        It's important all available bytes are consumed, because this call-back
        cannot keep up with incoming streams at real-time!
        """

        new_bytes = self.serial.readAll()

        for block in self.decoder.receive_bytes(bytearray(new_bytes)):
            self.update_data(*block)

    @pyqtSlot(QAction)
    def on_save(self, action: QAction):
        self.save_data(action.text())

    def save_data(self, file_format: str):
        """Save data, file_format is either `csv` or `numpy`"""

        file_format = file_format.lower()

        # Abort export if the #channels (rows in self.data) is 0
        if np.size(self.data, 0) == 0:
            message = QMessageBox()
            QMessageBox.information(message, 'Saving data',
                                    'No data recorded yet', QMessageBox.Ok)
            return

        if file_format == 'numpy':
            ext = 'Numpy Data (*.npz)'
        else:
            ext = 'Comma Separated Values (*.csv)'

        options = QFileDialog.Options()
        filename, _ = QFileDialog.getSaveFileName(
            self, 'QFileDialog.getSaveFileName()', '', ext,
            options=options)

        if filename:
            if file_format == 'numpy':
                np.savez(filename, data=self.data, time=self.time)
            else:
                data = np.vstack((self.time, self.data))
                header = 'time [s]'
                for i in range(self.channels):
                    header += ', Channel {}'.format(i)

                np.savetxt(filename, data.transpose(),
                           delimiter=';', header=header)

    def load_settings(self):
        """Load settings from file"""
        try:
            with open('settings.json', 'r') as file:
                settings = json.load(file)
                if 'port' in settings and settings['port']:
                    self.input_port.setCurrentIndex(
                        self.input_port.findData(settings['port'])
                    )
                if 'size' in settings and settings['size'] > 10:
                    self.input_size.setText(str(settings['size']))
                if 'overlay' in settings:
                    self.input_overlay.setChecked(settings['overlay'])
                if 'autoscale' in settings:
                    self.input_autoscale.setChecked(settings['autoscale'])
                if 'y_scale_max' in settings:
                    self.input_scale['max'].\
                        setText(str(settings['y_scale_max']))
                if 'y_scale_min' in settings:
                    self.input_scale['min'].\
                        setText(str(settings['y_scale_min']))
        except FileNotFoundError:
            return  # Do nothing
        except json.decoder.JSONDecodeError:
            return  # Do nothing

    def save_settings(self):
        """Save current settings to file"""
        settings = {
            'port': self.serial.portName(),
            'size': self.data_size,
            'overlay': self.overlay,
            'autoscale': self.autoscale,
            'y_scale_min': self.y_scale[0],
            'y_scale_max': self.y_scale[1]
        }
        with open('settings.json', 'w') as file:
            file.write(json.dumps(settings))

    def closeEvent(self, event: QCloseEvent):
        """When main window is closed"""
        self.serial.close()
        self.save_settings()
        super().closeEvent(event)  # Call original method too

    def find_devices(self):
        """Set found serial devices into dropdown"""
        ports = QSerialPortInfo.availablePorts()

        self.input_port.clear()

        for port in ports:

            label = port.portName()
            if port.description:
                label += ' - ' + port.description()

            self.input_port.addItem(label, port.portName())

    def start_recording(self):
        """Called when recording should start (e.g. when `Connect` was hit)"""
        self.channels = 0  # Force an update on the next data point
        self.data_points = 0
        self.data_size = int(self.input_size.text())
        self.overlay = self.input_overlay.isChecked()
        self.autoscale = self.input_autoscale.isChecked()
        self.y_scale = [
            float(self.input_scale['min'].text()),
            float(self.input_scale['max'].text())
        ]

        self.serial.clear()  # Get rid of data in buffer

    def update_data(self, channels: int, micros: int, new_data: list):
        """Called when new row was received"""

        if self.channels != channels:
            self.set_channels(channels)

        col = np.array(new_data, dtype=float)
        self.data = np.roll(self.data, -1, axis=1)  # Rotate backwards
        self.data[:, -1] = col[:]  # Set new column at the end

        self.time = np.roll(self.time, -1)  # Rotate backwards

        if self.time_offset is None:
            self.time_offset = micros

        self.time[0, -1] = (micros - self.time_offset) / 1000000

        self.data_points += 1

        now = time.time()
        if now - self.last_update >= self.FRAME_TIME:  # Limit update rate
            self.update_plots()
            self.last_update = now

    def update_plots(self):
        """With data already updated, update plots"""

        if self.data_points < self.data_size:
            data_x = self.time[:, -self.data_points:]
            data_y = self.data[:, -self.data_points:]
        else:
            data_x = self.time
            data_y = self.data

        for i, curve in enumerate(self.curves):
            curve.setData(x=data_x[0, :], y=data_y[i, :])

    def set_channels(self, channels: int):
        """
        Resize number of channels

        Also functions as a reset between recordings, also sets new plot
        windows and curves
        """

        self.channels = channels
        self.data = np.zeros((channels, self.data_size))
        self.time = np.zeros((1, self.data_size))
        self.time_offset = None
        self.data_points = 0

        self.last_update = 0

        self.create_plots()

    def create_plots(self):
        """Create the desired plots and curves"""

        # Clear old
        for plot in self.plots:
            plot.clear()
            self.layout_plots.removeItem(plot)

        self.plots = []
        self.curves = []

        if self.overlay:
            new_plot = self.layout_plots.addPlot(row=0, col=0,
                                                 title='Channels')

            for i in range(self.channels):
                new_curve = new_plot.plot()
                self.curves.append(new_curve)

            self.plots.append(new_plot)

        else:
            for i in range(self.channels):
                new_plot = self.layout_plots.addPlot(row=i, col=0,
                                                     title='Ch{}'.format(i))
                new_plot.showGrid(False, True)

                new_curve = new_plot.plot()

                self.plots.append(new_plot)
                self.curves.append(new_curve)

        # Set style
        for plot in self.plots:
            plot.showGrid(False, True)
            if not self.autoscale:
                # Autoscaling is by default
                plot.setYRange(self.y_scale[0], self.y_scale[1])

        for i, curve in enumerate(self.curves):
            # Set automatic colors
            c = self.LINECOLORS[i % len(self.LINECOLORS)]
            pen = pg.mkPen(c, width=2)
            curve.setPen(pen)
