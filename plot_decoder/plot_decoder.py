from enum import Enum
import struct
from typing import Iterator, Tuple


class Mode(Enum):
    HEADER = 1
    SIZE = 2
    TIME = 3
    DATA = 4


class PlotDecoder:
    """Class to decode the BioRobotics plot protocol.

    The serial protocol is as follows:

    First the header (3 bytes): 7f ff bf
    Then the channel count (1 byte): 03
    Then the microtime (4 bytes, signed integer): 00 01 01 01
    Then follow the floats (4 bytes each): 01 01 01 01 ...
    """

    HEADER_BYTES = bytearray(b'\x7f\xff\xbf')

    def __init__(self, *args, **kwargs):
        """Constructor."""

        # Keep internals of the last received data

        self._buffer = bytearray()  # Store bytes while receiving
        self._mode = Mode.HEADER
        self._bytes_count = 0  # Number of bytes in current mode

        self._channel_size = 0  # Number of channels
        self._time = 0  # Read microtime

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, new_mode: Mode):
        """Simple wrapper to change mode."""

        self._mode = new_mode
        self._bytes_count = 0
        self._buffer = bytearray()

    def receive_bytes(self, new_bytes: bytearray) -> Iterator[Tuple]:
        """Precess a set of incoming bytes.

        Multiple packages can be received at once. The data stream can be distributed
        over any number of method calls.
        This method acts as a generator, yielding a list of tuples
        like (n_channels, time, data).
        """

        i = 0  # Index inside set of bytes

        new_size = len(new_bytes)

        while i < new_size:

            if self._mode == Mode.HEADER:  # If header not passed

                # Increment header
                if new_bytes[i] == self.HEADER_BYTES[self._bytes_count]:
                    self._bytes_count += 1
                else:
                    self._bytes_count = 0  # Reset, header failed

                if self._bytes_count >= 3:  # Header completed
                    self.mode = Mode.SIZE

                i += 1
                continue

            if self._mode == Mode.SIZE:
                self._channel_size = new_bytes[i]
                self.mode = Mode.TIME

                i += 1
                continue

            if self._mode == Mode.TIME:

                # Copy until buffer contains 4 bytes or until new segment is depleted
                copy_bytes = min(4 - len(self._buffer), new_size - i)
                self._buffer.extend(new_bytes[i:(i + copy_bytes)])

                if len(self._buffer) == 4:
                    self._time = int.from_bytes(self._buffer, byteorder='big', signed=True)
                    self.mode = Mode.DATA
                elif len(self._buffer) > 4:
                    self.mode = Mode.HEADER  # Something went wrong

                i += copy_bytes
                continue

            if self._mode == Mode.DATA:

                # Copy until buffer contains 4 bytes or until new segment is depleted
                copy_bytes = min(self._channel_size * 4 - len(self._buffer), new_size - i)

                self._buffer.extend(new_bytes[i:(i + copy_bytes)])

                # If enough bytes are collected for all nch floats
                if len(self._buffer) == self._channel_size * 4:

                    # Decode at once
                    values = struct.unpack(self._channel_size * 'f', self._buffer)
                    data = list(values)  # Tuple to list

                    # Yield new package
                    yield self._channel_size, self._time, data

                    self.mode = Mode.HEADER

                elif len(self._buffer) > self._channel_size * 4:
                    self.mode = Mode.HEADER  # Something went wrong

                i += copy_bytes
                continue
